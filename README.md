proxyscan
=========

ProxyScan is an HTTP Proxy Scanner that scans random IP addresses looking
for open proxy servers.

NOTE: It is not advisable to run an open proxy- they are easily abused. This
tool is only to be used by network administrators to detect open proxies so
that their operators can be warned to shut them down.

![proxyscan-screenshot.png](proxyscan-screenshot.png "Screenshot of ProxyScan in action.")

Downloads:
----------
Clone the repo:

$ git clone 'https://github.com/EricShalov/proxyscan.git'

License:
--------
EricDraw is licensed under the <a href="http://opensource.org/licenses/BSD-3-Clause">BSD 3-Clause License</a>.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the {organization} nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.
